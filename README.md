# AppleDesktopWallpaperDownloader

Downloads MacOS default wallpapers because they _try_ to make it hard for us. ( •̀_•́ )

Files are downloaded in parallel by default to a folder called `wallpapers` in the current directory. As of April 2024, the downloaded zip files take up about 1.63GB of disk space.

Note: The provided XML file is optional and is provided in case Apple moves the default location from `/System/Library/AssetsV2/` again and/or your don't have the file in the specificed.

## Setup and installation

```
$ python3 -m venv .venv
$ source .venv/bin/activate
$ pip install -r requirements.txt
$ .venv/bin/deactivate # to exit virtual environment
```

## Usage

```
python3 ./download-wallpapers.py
```

#  Updating requirements.txt

```
pip freeze > requirements.txt
```

## References

[https://beautiful-soup-4.readthedocs.io/en/latest/#](https://beautiful-soup-4.readthedocs.io/en/latest/#)

[https://realpython.com/python-download-file-from-url/](https://realpython.com/python-download-file-from-url/)

[https://www.studytonight.com/python-howtos/how-to-read-xml-file-in-python](https://www.studytonight.com/python-howtos/how-to-read-xml-file-in-python)


## License

MIT License
