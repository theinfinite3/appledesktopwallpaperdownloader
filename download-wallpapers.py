from bs4 import BeautifulSoup, Tag
import aiohttp
import asyncio
import os

# Downloads MacOS default wallpapers
#
# References:
# https://beautiful-soup-4.readthedocs.io/en/latest/#
# https://realpython.com/python-download-file-from-url/
# https://www.studytonight.com/python-howtos/how-to-read-xml-file-in-python
#
# Setup and installation
# python3 -m venv .venv
# source .venv/bin/activate
# pip install -r requirements.txt
#
# Running and 
# python3 ./download-wallpapers.py
#
# Updating requirements
# pip freeze > requirements.txt

# File containing metadata about the files
FILE='/System/Library/AssetsV2/com_apple_MobileAsset_DesktopPicture/com_apple_MobileAsset_DesktopPicture.xml'
DOWNLOAD_DIR="wallpapers"

try:
    with open(FILE) as f:
        data = f.read()
except FileNotFoundError:
    Log.error("File not found. Try using the .xml file located in this directory?")
    sys.exit()

soup = BeautifulSoup(data, 'xml')

asset_list = soup.array
asset_map = {}

for item in asset_list.descendants:
    if not isinstance(item, Tag):
        continue

    item_text = item.get_text()
    if not item_text == 'DesktopPictureID':
        continue

    item_map = {
        'name': '',
        'base_url': '',
        'relative_path': ''
    }
    if item_text == 'DesktopPictureID':
        item_map.update({'name': item.find_next('string').get_text()})

        for sibling in item.next_siblings:
            if sibling.get_text() == '__BaseURL':
                print(sibling.get_text())
                item_map.update({'base_url': sibling.find_next('string').get_text()})

            if sibling.get_text() == '__RelativePath':
                print(sibling.get_text())
                item_map.update({'relative_path': sibling.find_next('string').get_text()})
    
        asset_map[item_map.get('name')] = item_map

async def download_file(url, name=''):
    async with aiohttp.ClientSession() as session:
        async with session.get(url) as response:
            if "content-disposition" in response.headers:
                header = response.headers["content-disposition"]
                filename = header.split("filename=")[1]
            elif name:
                filename = name
            else:
                filename = url.split("/")[-1]
            with open(filename, mode="wb") as file:
                while True:
                    chunk = await response.content.read()
                    if not chunk:
                        break
                    file.write(chunk)
                print(f"Downloaded file {filename}")

async def main():
    if not os.path.isdir(DOWNLOAD_DIR):
        os.makedirs(DOWNLOAD_DIR)
    os.chdir(DOWNLOAD_DIR)
    tasks = [download_file(asset.get('base_url') + asset.get('relative_path'), asset.get('name') + '.zip') for asset in asset_map.values()]
    await asyncio.gather(*tasks)

asyncio.run(main())
